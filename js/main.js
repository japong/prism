const controlButtons = document.querySelectorAll('.control-button');

controlButtons.forEach(function(btn) {
  btn.addEventListener('click', function() {
    var classAdd = this.innerHTML;
    var element = document.getElementById("stage");
    element.className = "";
    element.classList.add(classAdd);
  });
  
});

const toggleButton = document.querySelector('.toggle-button');
const prismGrow = document.querySelector('#nose-bottom');
toggleButton.onclick = () => prismGrow.classList.toggle('grow');